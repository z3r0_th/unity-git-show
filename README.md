To use the GitShow:

1. Add the package to your Unity package:
    * open manifest.json at Project/Packages
    * add the line 
        ```"com.matheusfernandestech.git-show": "https://gitlab.com/z3r0_th/unity-git-show.git#upm"```
      to the list of dependencies
    * Open/return to Unity3D
    * Unity will import the project/package
2. Now when you hit play and/or build your application, the GitShow will generate a file and a prefab at:
    * ```Assets/Resources/gitShow```
3. You may edit the versionBox prefab to add small configuration
    * The default is to show only at editor and debug/dev build
    * The default is "press Tab" to show the project version at bottom left corner of screen
    * The default, at Mobile, is the version always preset
4. You must have at least a git repository present with one commit. Otherwise the ProjectVersion won't have any version to show
5. Add ```Resources/gitShow``` to your gitignore file you don't need to version it, as it is updated with git status
    * If you version it, it might be always dirty - When you commit it will be updated, and than you commit (the version file changes) and it will need update again  

5. You may use the static properties from ProjectVersion to show the version in any other place. 

Note: The FullVersion property will return a tag, if any, or the commit hash (with -dirty appended if dirty). 
The tag uses the git function:
```git describe --tags --dirty```
so it shows:
 1. only the release tag, if commit equals tag
 2. release tag plus commit hash if the commit is ahead of tag. Something like this ```v0.0.2-7-g77000f8```, where ```77000f8``` is the hash
 3. release tag plus commit hash plus dirty append if there's something changed in code
 
I hope these information might help you to keep track of builds and modifications as you distribute the versions for internal testing

(icon from flaticons https://www.flaticon.com/authors/flat-icons)
