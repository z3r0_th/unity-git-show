﻿using GitShow;
using UnityEngine;

public class Testing : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Tag: " + ProjectVersion.Tag);
        Debug.Log("Dirty: " + ProjectVersion.Dirty);
        Debug.Log("Branch: " + ProjectVersion.Branch);
        Debug.Log("Hash: " + ProjectVersion.Hash);
        Debug.Log("Date: " + ProjectVersion.FullDate);
        Debug.Log("Day: " + ProjectVersion.DateTime.Day);
        Debug.Log("Mon: " + ProjectVersion.DateTime.Month);
        Debug.Log("Hour: " + ProjectVersion.DateTime.Hour);
        Debug.Log("Year: " + ProjectVersion.DateTime.Year);
    }
}
