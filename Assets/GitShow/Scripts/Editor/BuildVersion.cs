﻿using UnityEditor.Build.Reporting;
using UnityEditor.Build;
using UnityEditor;
using UnityEngine;

namespace GitShow.Scripts
{
    public static class BuildVersion
    {
        [InitializeOnEnterPlayMode]
        public static void UpdateVersionFile()
        {
            var projectFile = ProjectVersion.Instance;
            var previousVersion = "";

            previousVersion = ProjectVersion.Hash;
            var serializedObject = new SerializedObject(projectFile);
            serializedObject.UpdateIfRequiredOrScript();
            serializedObject.FindProperty("tag").stringValue = Version.Tag;
            serializedObject.FindProperty("branch").stringValue = Version.Branch;
            serializedObject.FindProperty("fullDate").stringValue = Version.FullDate;
            serializedObject.FindProperty("dirty").boolValue = Version.Dirty;
            serializedObject.FindProperty("hash").stringValue = Version.Hash;
            serializedObject.ApplyModifiedPropertiesWithoutUndo();

            if (previousVersion != ProjectVersion.Hash)
            {
                Debug.Log($"Version Update <color=blue>{ProjectVersion.FullVersion}</color>");            
            }
            else
            {
                Debug.Log($"Version <color=green>{ProjectVersion.FullVersion}</color>");
            }

            CreateSimpleGitShowPrefabIfNeeded();
        }

        private static void CreateSimpleGitShowPrefabIfNeeded()
        {
            var fileFolder = ProjectVersion.FileFolder;
            var path = $"{fileFolder}/versionBox";
            var gitShow = Resources.Load<SimpleGitShow>(path);
            if (gitShow == null)
            {
                var go = new GameObject("versionBox");
                go.AddComponent<SimpleGitShow>();
                PrefabUtility.SaveAsPrefabAsset(go, "Assets/Resources/" + path + ".prefab");
                Debug.Log("<i><color=green>SimpleGitShow prefab created</color></i>");
                GameObject.DestroyImmediate(go);
            }
        }
    }
    
    class MyCustomBuildProcessor : IPreprocessBuildWithReport
    {
        public int callbackOrder => 0;
        public void OnPreprocessBuild(BuildReport report)
        {
            BuildVersion.UpdateVersionFile();
        }
    }
}
