﻿using System;

namespace GitShow
{
    public static class Version
    {
        public static string GitVersion => GitUtility.RunGitCommand("--version", ""); 
        public static string Tag => GitUtility.RunGitCommand("describe", "--tags --dirty");

        public static string Branch => GitUtility.RunGitCommand("branch", "--show-current");
        
        public static string Hash =>  GitUtility.RunGitCommand("rev-parse", " --short HEAD");

        public static string FullDate => GitUtility.RunGitCommand("log", " -1 --format=%cd");
        
        public static bool Dirty
        {
            get
            {
                var isDirty = GitUtility.RunGitCommand("diff", "--stat");
                return !string.IsNullOrEmpty(isDirty);
            }
        }

        public static DateTime Date
        {
            get
            {
                var date = FullDate;
                
                var dateSplit = date.Split(new []{" "}, StringSplitOptions.RemoveEmptyEntries);
                // Mon Feb 8 22:48:04 2021 -0300
                var month = dateSplit[1];
                var day = dateSplit[2];
                var year = dateSplit[4];
                var hour = dateSplit[3].Split(new[] {":"}, StringSplitOptions.RemoveEmptyEntries)[0];
                return new DateTime(int.Parse(year), Month(month), int.Parse(day), int.Parse(hour), 0, 0);
            }
        }

        private static int Month(string m)
        {
            switch (m)
            {
                case "Jan": return 0;
                case "Feb": return 1;
                case "Mar": return 2;
                case "Apr": return 3;
                case "May": return 4;
                case "Jun": return 5;
                case "Jul": return 6;
                case "Aug": return 7;
                case "Sep": return 8;
                case "Oct": return 9;
                case "Nov": return 10;
                case "Dec": return 11;
            }

            return -1;
        }
    }
}
