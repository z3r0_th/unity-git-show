﻿using System;
using System.Diagnostics;

namespace GitShow
{
    public static class GitUtility
    {
        // method from :https://stackoverflow.com/questions/45202896/how-to-checkout-tags-using-c-sharp-and-git
        public static string RunGitCommand(string command, string args=""/*, string workingDirectory*/)
        {
            string git = "git";
            var results = "";
            var proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = git,
                    Arguments = $"{command} {args}",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true,
                    //WorkingDirectory = workingDirectory,
                }
            };
            try
            {
                proc.Start();
                while (!proc.StandardOutput.EndOfStream)
                {
                    results += $"{proc.StandardOutput.ReadLine()},";
                }

                if (!string.IsNullOrEmpty(results)) results = results.Remove(results.Length - 1, 1);
            }
            catch(Exception e)
            {
                UnityEngine.Debug.Log($"Could not parse git command <b>{command}</b> <i>{args}</i> - {e}");
            }
            finally
            {
                proc.WaitForExit();                
            }
            
            return results;
        }
    }
}