﻿using System;
using UnityEngine;

namespace GitShow
{
    public class ProjectVersion : ScriptableObject
    {
        public static readonly string FileFolder = "gitShow";
        private static ProjectVersion instance;

        public static ProjectVersion Instance
        {
            get
            {
                if (instance == null)
                {
                    var projectFile = Resources.Load<ProjectVersion>($"{FileFolder}/version");
                    if (projectFile == null)
                    {
                        projectFile = CreateInstance<ProjectVersion>();
                        #if UNITY_EDITOR
                            if (!System.IO.Directory.Exists($"Assets/Resources/{FileFolder}")) System.IO.Directory.CreateDirectory($"Assets/Resources/{FileFolder}");
                            UnityEditor.AssetDatabase.CreateAsset(projectFile, $"Assets/Resources/{FileFolder}/version.asset");
                        #endif
                    }

                    instance = projectFile;
                }

                return instance;
            }
        }

        public static string Branch => Instance.branch;
        public static string FullDate => Instance.fullDate;
        public static string Hash => Instance.hash;
        public static bool Dirty => Instance.dirty;
        public static string Tag => Instance.tag;

        public static string FullVersion
        {
            get
            {
                var currentVersion = Tag;
                if (!string.IsNullOrEmpty(currentVersion)) return currentVersion;
                return Hash + (Dirty?"-dirty":"");
            }
        }

        public static bool ShowSimpleGitShowRelease => Instance.useSimpleGitShowOnRelease;
        
        public static bool ShowSimpleGitShowDebug => Instance.useSimpleGitShowOnDebug;

        public static DateTime DateTime
        {
            get
            {
                if (Instance.timeSetup) return Instance.time;
                
                var date = FullDate;
                var dateSplit = date.Split(new []{" "}, StringSplitOptions.RemoveEmptyEntries);
                // Mon Feb 8 22:48:04 2021 -0300
                var month = dateSplit[1];
                var day = dateSplit[2];
                var year = dateSplit[4];
                var hour = dateSplit[3].Split(new[] {":"}, StringSplitOptions.RemoveEmptyEntries)[0];
                Instance.time = new DateTime(int.Parse(year), Month(month), int.Parse(day), int.Parse(hour), 0, 0);
                Instance.timeSetup = true;
                return Instance.time;
            }
        }

        private DateTime time;
        private bool timeSetup;
        
        [SerializeField] private string fullDate = "";
        [SerializeField] private string branch = "";
        [SerializeField] private string hash = "";
        [SerializeField] private string tag = "";
        [SerializeField] private bool dirty = false;
        
        [Header("Simple Git Show Box Config")]
        [Tooltip("Show a Version box on Editor and development build")]
        [SerializeField] private bool useSimpleGitShowOnDebug = true;
        [SerializeField] private bool useSimpleGitShowOnRelease = false;
        
        private static int Month(string m)
        {
            switch (m)
            {
                case "Jan": return 0;
                case "Feb": return 1;
                case "Mar": return 2;
                case "Apr": return 3;
                case "May": return 4;
                case "Jun": return 5;
                case "Jul": return 6;
                case "Aug": return 7;
                case "Sep": return 8;
                case "Oct": return 9;
                case "Nov": return 10;
                case "Dec": return 11;
            }

            return -1;
        }
    }
}
