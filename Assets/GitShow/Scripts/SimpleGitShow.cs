﻿using UnityEngine;

namespace GitShow
{
    public class SimpleGitShow : MonoBehaviour
    {
        public enum Corner
        {
            TopLeft,
            TopRight,
            BottomLeft,
            BottomRight
        }

        public Corner corner = Corner.BottomLeft;
        public bool useBackground = true;
        
        public KeyCode hotKey = KeyCode.Tab;
        public bool showOnlyWithHotKey = true;
        public bool keepOnScreenOnMobile = true;

        private bool showing = false;

        private void Start()
        { 
            DontDestroyOnLoad(gameObject);
            gameObject.hideFlags = HideFlags.HideAndDontSave;
        }

        private void Update()
        {
            bool showVersion = !showOnlyWithHotKey || showOnlyWithHotKey && Input.GetKey(hotKey);
            if (SystemInfo.deviceType == DeviceType.Handheld || Application.isMobilePlatform ||
                SystemInfo.deviceType == DeviceType.Unknown)
            {
                showVersion = keepOnScreenOnMobile;
            }
            
            showing = showVersion;
        }

        private void OnGUI()
        {
            if (!showing || string.IsNullOrEmpty(ProjectVersion.FullVersion)) return;
            var version = ProjectVersion.FullVersion;
            var labelWidth = GUIStyle.none.CalcSize(new GUIContent(version));
            var width = labelWidth.x;
            var height = labelWidth.y * 1.7f;
            var x = (corner == Corner.BottomLeft || corner == Corner.TopLeft) ? 0 : Screen.width - width;
            var y = (corner == Corner.TopLeft || corner == Corner.TopRight) ? 0 : Screen.height - height;
            Rect rect = new Rect(x,y,width,height);
            if (useBackground)
            {
                float pad = 3;
                rect.x = rect.x + ((corner == Corner.BottomLeft || corner == Corner.TopLeft)?pad:-pad);
                GUI.Box(new Rect(rect.x - pad, y, width+pad*2, height), "");
            }
            
            GUI.Label(rect, ProjectVersion.FullVersion);
        }

        [RuntimeInitializeOnLoadMethod]
        static void BePresent()
        {
            var fileFolder = ProjectVersion.FileFolder;
            var path = $"{fileFolder}/versionBox";
            if (ProjectVersion.ShowSimpleGitShowRelease || (ProjectVersion.ShowSimpleGitShowDebug && (Debug.isDebugBuild || Application.isEditor)))
            {
                var go = Resources.Load<SimpleGitShow>(path);
                if (go == null) return;
                Instantiate(go);
                Debug.Log("<i>Using SimpleGitShow</i>");
            }
        }
    }
}
